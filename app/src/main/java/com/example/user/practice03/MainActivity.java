package com.example.user.practice03;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.user.practice03.R;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class MainActivity extends AppCompatActivity {
    EditText movieName;
    Button btnSearch;
    ImageView movieImg;
    TextView movieTitle;
    TextView movieContent;
    String TAG = "server_response";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        movieName = (EditText) findViewById(R.id.movieName);
        btnSearch = (Button) findViewById(R.id.btnSearch);
        movieImg = (ImageView) findViewById(R.id.movieImg);
        movieTitle = (TextView) findViewById(R.id.movieTitle);
        movieContent = (TextView) findViewById(R.id.movieContent);

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String url = "http://www.omdbapi.com/?t=" + movieName.getText().toString();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        searchMovieByUrl(url);
                    }
                }).start();
            }
        });

    }

    public void searchMovieByUrl(String url) {
        try {
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", "Mozilla/5.0");
            int responseCode = con.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                StringBuffer response = new StringBuffer();
                String content;
                while ((content = in.readLine()) != null) {
                    response.append(content);
                }
                in.close();
                //System.out.println(response.toString());
                setMovieDetail(response.toString());
                // Log.d(TAG, response.toString());
            } else {
                System.out.println("GET request connection was not sucessfull!!");
            }

        } catch (Exception e) {
            Toast.makeText(this, "Web service error:" + e.getMessage().toString(), Toast.LENGTH_LONG).show();
        }


    }

    public void setMovieDetail(String response) {
        try {
            JSONObject allObjects = new JSONObject(response);
            final String title = allObjects.getString("Title");
            final String Url = allObjects.getString("Poster");
            final String content = allObjects.getString("Plot");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    movieTitle.setText(title);
                    Glide.with(MainActivity.this).load(Url).into(movieImg);
                    movieContent.setText(content);
                }
            });
        } catch (Exception e) {
            Toast.makeText(MainActivity.this, "An Error occured during parsing the json:" + e.getMessage(), Toast.LENGTH_LONG).show();
        }

    }
}
